#coding=utf-8
"""
Reinforcement learning maze example.

Red rectangle:          explorer.
Black rectangles:       hells       [reward = -1].
Yellow bin circle:      paradise    [reward = +1].
All other states:       ground      [reward = 0].

This script is the environment part of this example.
The RL is in RL_brain.py.

View more on my tutorial page: https://morvanzhou.github.io/tutorials/
"""
import numpy as np
import sys
import time

class Maze(object):
    MAZE_H = 20  # grid height
    MAZE_W = 20  # grid width
    TANK_HEALTHY = 5  # 坦克生命值上限
    HOME_HEALTHY = 5  # 基地生命值上限

    #读取石头位置.
    def readStonesArray(self):
        stonesFile = open(self.sStonesFileName, "r")
        self.stonesNumber = int(stonesFile.readline()) #是否是障碍物，0不是1是.

        self.stones = np.zeros((self.stonesNumber, 2), dtype=np.int)
        for i in range(self.stonesNumber):
            stoneLocate = stonesFile.readline()
            stoneLocateInt = list(map(int, stoneLocate.split()))
            self.stones[i][0] = stoneLocateInt[0]
            self.stones[i][1] = stoneLocateInt[1]
            self.isObstacle[self.stones[i][0]][self.stones[i][1]] = 1
        return

    def readHome(self):
        homeFile = open(self.sHomeFileName, "r")
        self.oval[0] = int(homeFile.readline())
        self.oval[1] = int(homeFile.readline())
        self.homeLength = int(homeFile.readline())
        self.homeWidth = int(homeFile.readline())
        for k in range(0, self.homeLength):
            for o in range(0, self.homeWidth):
                self.isObstacle[self.oval[0] + k][self.oval[1] + o] = 1

    #炮台位置文件的读取
    def readBatteryArray(self):
        batteryFile = open(self.sBatteryFileName, "r")
        self.batteryNumber = int(batteryFile.readline()) #炮台数量
        '''
        每个炮台5个属性，依次是x坐标,y坐标,炮台血量,炮台方向,炮台射程
        炮台只有4个方向分别是0-3代表上下左右
        '''
        self.batterys = np.zeros((self.batteryNumber, 5), dtype=np.int)
        for i in range(self.batteryNumber):
            batteryData = batteryFile.readline()
            batteryFileInt = list(map(int, batteryData.split()))
            self.batterys[i][0] = batteryFileInt[0] #横坐标
            self.batterys[i][1] = batteryFileInt[1] #纵坐标
            self.isObstacle[self.batterys[i][0]][self.batterys[i][1]] = 1
            self.batterys[i][2] = batteryFileInt[2] #炮台血量
            self.batterys[i][3] = batteryFileInt[3] #炮台方向
            self.batterys[i][4] = batteryFileInt[4] #炮台射程

        #batterysForLocation为某个坐标可以被某些炮台所攻击到,值为炮台的下标
        self.batterysForLocation = np.full((self.MAZE_W, self.MAZE_H, self.batteryNumber + 1), 0, dtype=np.int)
        for i in range(self.batteryNumber):
            px = self.batterys[i][0] #px,py为遍历设置炮弹设计范围的游标
            py = self.batterys[i][1]
            for j in range(1, self.batterys[i][4]):
                px += self.dri[self.batterys[i][3]][0]
                py += self.dri[self.batterys[i][3]][1]
                if px < 0 or px >= self.MAZE_W or py < 0 or py >= self.MAZE_H:
                    break
                if self.isObstacle[px][py] == 1: #遇到障碍物
                    break
                currentBatterySize = self.batterysForLocation[px][py][0] #当期该位置扫描到的在打击范围内的炮台数
                self.batterysForLocation[px][py][currentBatterySize + 1] = i
                self.batterysForLocation[px][py][0] += 1

    def __init__(self, workspace):
        self.oval = [0,0]
        self.workspace = workspace
        self.sHomeFileName = workspace + "./home.txt"
        self.sStonesFileName = workspace + "./stones.txt"  # 石头位置文件名
        self.sBatteryFileName = workspace + "./battery.txt"  # 炮台位置文件名
        super(Maze, self).__init__()
        self.action_space = ['u', 'd', 'l', 'r', 'ul', 'ur', 'dl', 'dr', 'fire']
        self.fire_action = 8
        self.nMoveAction = 8
        self.dri = [[0, -1], [0, 1], [-1, 0], [1, 0], [-1, -1], [-1, 1], [1, -1], [1, 1]]
        #self.dri = [[0, -1], [0, 1], [0, 0], [0, 0], [0, -1], [0, 1], [0, -1], [0, 1]]
        self.n_actions = len(self.action_space)
        self.n_features = 2
        self.t = 0
        self.tankAtackRange = 5
        #self.gone = np.zeros((50,50),dtype=np.int)
        self.isObstacle = np.zeros((self.MAZE_W, self.MAZE_H), dtype=np.int)
        self._build_maze()
        self.readStonesArray()
        self.readHome()
        self.readBatteryArray()
        self.canAtack()

    def canAtack(self):
        x = self.oval[0]
        y = self.oval[1]
        for i in range(self.homeLength):
            for j in range(self.homeWidth):

                for k in range(self.nMoveAction):
                    x = self.oval[0] + i
                    y = self.oval[1] + j
                    while True:
                        x += self.dri[k][0]
                        y += self.dri[k][1]
                        print(x, y)
                        if x < 0 or y < 0 or x >= self.MAZE_W or y >= self.MAZE_H or self.isObstacle[x][y] or \
                                (x - self.oval[0] - i) * (x - self.oval[0] - i) + (y - self.oval[1] - j) * (
                                y - self.oval[1] - j) > self.tankAtackRange * self.tankAtackRange:
                            break
                        self.les[x][y] = 1
    def _build_maze(self):
        self.rect = [2, 2]
        self.les = np.zeros((self.MAZE_W, self.MAZE_H), dtype=np.int)
        self.bullet = 10
        self.health = 5


    def reset(self, x = 0, y = 0):
        self.stepp = 0
        self.rect = [x, y]
        self.bullet = 10
        self.health = self.HOME_HEALTHY
        self.tankHealthy = self.TANK_HEALTHY
        #self.gone = np.zeros((50, 50), dtype=np.int)
        a = np.array(self.rect)
        a = np.append(a, self.health)
        a = np.append(a, self.tankHealthy)
        return a

    def step(self, action):
        self.stepp += 1
        done = False
        reward = 0
        s = np.array(self.rect)
        if(action == self.fire_action):
            if self.les[s[0]][s[1]] == 1:
                reward = 1000
                self.bullet -= 1
                self.health -= 1
            else:
                reward = -1000
            if (self.health == 0):
                reward = 100000
                done = True
            #print(reward , " " , self.stepp , "[" , self.rect[0] , "," , self.rect[1] , "]" )
            a = np.array(self.rect)
            s_ = np.append(a, self.health)
            s_ = np.append(s_, self.tankHealthy)
            return s_, reward, done

        base_action = self.dri[action]
        s[0] += base_action[0]
        s[1] += base_action[1]

        distance = abs(s[0] - self.oval[0]) + abs(s[1] - self.oval[1])
        if distance < abs(self.rect[0] - self.oval[0]) + abs(
            self.rect[1] - self.oval[1]):
            reward = 100
        else:
            reward = -100
        if s[0] < 0 or s[0] >= self.MAZE_W or s[1] < 0 or s[1] >= self.MAZE_H:
            reward = -1000
        elif self.isObstacle[s[0]][s[1]] == 1: #是障碍物
            reward = -1000
        else:
            self.rect[0] += base_action[0]
            self.rect[1] += base_action[1]  # move agent
        # if self.gone[self.rect[0]][self.rect[1]] == 1:
        #     reward = -1000
        #print(reward , " " , self.stepp , "[" , self.rect[0] , "," , self.rect[1] , "]   [" , s[0] , "," , s[1] , "]");
        # reward function
        done = False
        a = np.array(self.rect)
        s_ = np.append(a, self.health)
        s_ = np.append(s_, self.tankHealthy)
        # print(self.rect, action, done, reward)
        #if self.stepp > maxStep:
           # done = True
        return s_, reward, done


    def can_action(self, observation):
        actions = [0, 1, 2, 3, 4, 5, 6, 7]
        row, col = observation
        if row == 0:
            actions.remove(0)
            actions.remove(4)
            actions.remove(5)
        elif row == self.MAZE_H - 1:
            actions.remove(1)
            actions.remove(6)
            actions.remove(7)

        if col == 0:
            actions.remove(2)
            actions.remove(4)
            actions.remove(6)
        elif col == self.MAZE_W - 1:
            actions.remove(3)
            actions.remove(5)
            actions.remove(7)

        # if row>0 and self.maze[row-1,col] == 0.0:
        #     actions.remove(1)
        # if row<MAZE_H-1 and self.maze[row+1,col] == 0.0:
        #     actions.remove(3)
        #
        # if col>0 and self.maze[row,col-1] == 0.0:
        #     actions.remove(0)
        # if col<MAZE_W-1 and self.maze[row,col+1] == 0.0:
        #     actions.remove(2)
        # 删除不可达动作
        return actions  # 返回可用动作

if __name__ == "__main__":
    maze = Maze()
    while True:
        a = np.random.randint(0, 8)
        s, r, done = maze.step(a)
        if done:
            break

    print("over")
