#!/usr/bin/python3
#!/usr/bin/env python3
from env import Maze
from DQN import DQN
import sys
import time

def run_maze():
    step = 0
    #e=0;
    episode = 0
    while True:
        # initial observation
        observation = env.reset()
        while True:
            action = RL.egreedy_action(observation)

            observation_, reward, done = env.step(action)
            '''if reward < 0:
                e += 1'''
            RL.perceive(observation, action, reward, observation_, done)

            observation = observation_
            step += 1
            # break while loop when end of this episode
            if done:
                print("当前所用步数", step)
                e=0
                RL.onStepDone()
                RL.saveStep(episode, step)
                if RL.outFile(step):
                    print('game over')
                    return
                step = 0
                episode += 1
                break

if __name__ == "__main__":
    # maze game
    global workSpace
    workSpace = ""
    order = ""
    if len(sys.argv) > 1:
        order = sys.argv[len(sys.argv) - 2]
        if order == "workspace":
            workSpace = sys.argv[len(sys.argv) - 1]
    print("order:", order)
    print("WorkSpace:", workSpace)
    env = Maze(workSpace)
    RL = DQN(workSpace)
    RL.setEnv(env)
    run_maze()