#coding=utf-8
import tensorflow.compat.v1 as tf
import numpy as np
import os
from collections import deque
from tensorflow import keras
from env import Maze
import random
tf.disable_eager_execution()

GAMMA = 0.9 # 目标Q的折现系数
INITIAL_EPSILON = 0.5 # ε的起始值
FINAL_EPSILON = 0.01 # ε的最终值
REPLAY_SIZE = 1000 # 体验重播缓冲区大小
BATCH_SIZE = 50 # 小批量的大小

# class WindeDeepModel(keras.models.Model):
#     def __init__(self):
#         super(WindeDeepModel, self).__init__()
#         # 定义模型的层次
#         self.hidden1_layer = keras.layers.Dense(500, activation='relu')
#         self.hidden2_layer = keras.layers.Dense(500, activation='relu')
#         self.hidden3_layer = keras.layers.Dense(500, activation='relu')
#         self.output_layer = keras.layers.Dense(8)
#
#     def call(self, inputs):
#         # 完成模型的正向计算
#         hidden1 = self.hidden1_layer(inputs)
#         hidden2 = self.hidden2_layer(hidden1)
#         hidden3 = self.hidden2_layer(hidden2)
#         concat = keras.layers.concatenate([inputs, hidden3])
#         output = self.output_layer(concat)
#         return output

class DQN:
    def __init__(self, workSpace = ""):  # 初始化
        self.isInitStep = False
        self.workspace = workSpace
        self.closeFileName = workSpace + "closeName.txt"
        self.outStepFileName = workSpace + "step.txt"
        self.replay_buffer = deque()
        self.time_step = 0
        self.epsilon = INITIAL_EPSILON
        self.state_dim = 4
        self.action_dim = 9
        self.answer = ""

        self.create_Q_network()
        self.create_training_method()

        self.session = tf.InteractiveSession()
        self.session.run(tf.initialize_all_variables())
        self.env = 0
        self.answerMap = 0

    def create_Q_network(self):  # 创建Q网络
        """首先定义各层的权重和偏置"""
        W1 = self.weight_variable([self.state_dim, 2500])
        b1 = self.bias_variable([2500])
        W2 = self.weight_variable([2500, 500])
        b2 = self.bias_variable([500])
        W3 = self.weight_variable([500, 50])
        b3 = self.bias_variable([50])
        W4 = self.weight_variable([50, self.action_dim])
        b4 = self.bias_variable([self.action_dim])

        # input layer
        self.state_input = tf.placeholder("float", [None, self.state_dim])
        # hidden layers
        h_layer = tf.nn.relu(tf.matmul(self.state_input, W1) + b1)
        h_norm = tf.layers.batch_normalization(
            h_layer,
            axis=-1,
            momentum=0.99,
            epsilon=0.001,
            center=True,
            scale=True,
            beta_initializer=tf.zeros_initializer(),
            gamma_initializer=tf.ones_initializer(),
            moving_mean_initializer=tf.zeros_initializer(),
            moving_variance_initializer=tf.ones_initializer(),
            beta_regularizer=None,
            gamma_regularizer=None,
            beta_constraint=None,
            gamma_constraint=None,
            training=False,
            trainable=True,
            name=None,
            reuse=None,
            renorm=False,
            renorm_clipping=None,
            renorm_momentum=0.99,
            fused=None,
            virtual_batch_size=None,
            adjustment=None
        )
        # Q Value layer
        h_layer2 = tf.nn.relu(tf.matmul(h_norm, W2) + b2)
        h_norm2 = tf.layers.batch_normalization(
            h_layer2,
            axis=-1,
            momentum=0.99,
            epsilon=0.001,
            center=True,
            scale=True,
            beta_initializer=tf.zeros_initializer(),
            gamma_initializer=tf.ones_initializer(),
            moving_mean_initializer=tf.zeros_initializer(),
            moving_variance_initializer=tf.ones_initializer(),
            beta_regularizer=None,
            gamma_regularizer=None,
            beta_constraint=None,
            gamma_constraint=None,
            training=False,
            trainable=True,
            name=None,
            reuse=None,
            renorm=False,
            renorm_clipping=None,
            renorm_momentum=0.99,
            fused=None,
            virtual_batch_size=None,
            adjustment=None
        )
        h_layer3 = tf.nn.relu(tf.matmul(h_norm2, W3) + b3)
        h_norm3 = tf.layers.batch_normalization(
            h_layer3,
            axis=-1,
            momentum=0.99,
            epsilon=0.001,
            center=True,
            scale=True,
            beta_initializer=tf.zeros_initializer(),
            gamma_initializer=tf.ones_initializer(),
            moving_mean_initializer=tf.zeros_initializer(),
            moving_variance_initializer=tf.ones_initializer(),
            beta_regularizer=None,
            gamma_regularizer=None,
            beta_constraint=None,
            gamma_constraint=None,
            training=False,
            trainable=True,
            name=None,
            reuse=None,
            renorm=False,
            renorm_clipping=None,
            renorm_momentum=0.99,
            fused=None,
            virtual_batch_size=None,
            adjustment=None
        )
        self.Q_value = tf.matmul(h_norm3, W4) + b4
        # 隐藏层1张量
        # model = keras.Sequential([
        #     keras.Dense(256, activation=tf.nn.relu),
        #     keras.Dense(128, activation=tf.nn.relu),
        #     keras.Dense(64, activation=tf.nn.relu),
        #     keras.Dense(8, activation=None)
        # ])
        # 前向运算只需要调用一次网络大类对象即可所有层的顺序计算
        # out = model(x)  # 前向计算得到输出

    def weight_variable(self, shape):
        initial = tf.truncated_normal(shape)
        return tf.Variable(initial)

    def bias_variable(self, shape):
        initial = tf.constant(0.01, shape=shape)
        return tf.Variable(initial)

    def create_training_method(self):  # 创建训练方法
        self.action_input = tf.placeholder("float", [None, self.action_dim])  # one hot presentation
        self.y_input = tf.placeholder("float", [None])
        Q_action = tf.reduce_sum(tf.multiply(self.Q_value, self.action_input), reduction_indices=1)
        self.cost = tf.reduce_mean(tf.square(self.y_input - Q_action))
        self.optimizer = tf.train.AdamOptimizer(0.0001).minimize(self.cost)

    def perceive(self, state, action, reward, next_state, done):  # 感知存储信息
        one_hot_action = np.zeros(self.action_dim)
        one_hot_action[action] = 1
        self.replay_buffer.append((state, one_hot_action, reward, next_state, done))
        if len(self.replay_buffer) > REPLAY_SIZE:
            self.replay_buffer.popleft()

        if len(self.replay_buffer) > BATCH_SIZE:
            self.train_Q_network()

    def onStepDone(self):
        if self.epsilon > FINAL_EPSILON:
            self.epsilon = self.epsilon * 0.99
        else:
            self.epsilon = 0
        print("当前随机概率", self.epsilon)

    def train_Q_network(self):  # 训练网络
        self.time_step += 1
        # Step 1: obtain random minibatch from replay memory
        minibatch = random.sample(self.replay_buffer, BATCH_SIZE)
        state_batch = [data[0] for data in minibatch]
        action_batch = [data[1] for data in minibatch]
        reward_batch = [data[2] for data in minibatch]
        next_state_batch = [data[3] for data in minibatch]

        # Step 2: calculate y
        y_batch = []
        Q_value_batch = self.Q_value.eval(feed_dict={self.state_input: next_state_batch})
        for i in range(0, BATCH_SIZE):
            done = minibatch[i][4]
            if done:
                y_batch.append(reward_batch[i])
            else:
                y_batch.append(reward_batch[i] + GAMMA * np.max(Q_value_batch[i]))

        self.optimizer.run(feed_dict={
            self.y_input: y_batch,
            self.action_input: action_batch,
            self.state_input: state_batch
        })
    def egreedy_action(self, state):  # 输出带随机的动作
        Q_value = self.Q_value.eval(feed_dict={
            self.state_input: [state]
        })[0]
        #print(state, Q_value)
        rand = random.randint(0, self.action_dim - 1)
        if random.random() <= self.epsilon:
            return rand
        else:
            return np.argmax(Q_value)

    def action(self, state):  # 输出动作
        return np.argmax(self.Q_value.eval(feed_dict={
            self.state_input: [state]
        })[0])

    def createAnswer(self): #在新的一轮前把最优表保存在内存中
        if not isinstance(self.env, Maze):
            return
        self.answer = ""
        for i in range(self.env.MAZE_W):
            for j in range(self.env.MAZE_H):
                for k in range(0, self.env.HOME_HEALTHY + 1):
                    for l in range(0, self.env.TANK_HEALTHY + 1):
                        Q_v = self.Q_value.eval(feed_dict={
                            self.state_input: [[i, j, k, l]]
                        })[0]
                        self.answer = self.answer + str(i) + ' '
                        self.answer = self.answer + str(j) + ' '
                        self.answer = self.answer + str(k) + ' '
                        self.answer = self.answer + str(l) + ' '
                        for action in range(self.action_dim):
                            self.answer = self.answer + str(Q_v[action])
                            if (action + 1 != self.action_dim):
                                self.answer = self.answer + ' '
                            else:
                                self.answer = self.answer + '\n'

    def outFile(self, step):  # 输出动作
        if not os.path.exists(self.closeFileName):
            print("close file not exists")
            return False
        fo = open(self.closeFileName, "r")
        times = int(fo.readline())
        print("times:", times, "step:", step)
        if (step > times):
            return False
        self.createAnswer()
        fo = open(self.workspace + "way.txt", "w")
        fo.write(self.answer)
        fo.close()
        return True

    def saveStep(self, episode, step):  # 输出动作
        if (not self.isInitStep):
            self.initStep()
        fo = open(self.outStepFileName, "a")
        fo.write(str(step))
        fo.write("\n")
        fo.close()

    def initStep(self):  # 输出动作
        fo = open(self.outStepFileName, "w")
        fo.close()
        self.isInitStep = True

    def reEpsilon(self):
        self.epsilon=INITIAL_EPSILON

    def setEnv(self, env):
        self.env = env